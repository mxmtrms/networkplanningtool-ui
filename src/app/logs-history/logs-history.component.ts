import {Component, OnInit} from '@angular/core';
import {Log} from '../_helpers/log';
import {LogService} from '../_service/log.service';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-logs-history',
  templateUrl: './logs-history.component.html',
  styleUrls: ['./logs-history.component.css']
})
export class LogsHistoryComponent implements OnInit {
  logUrl = `${environment.apiUrl}/models/:id/export`;
  logs: Log[];
  modalOpened = false;
  yangOpened = false;
  fileData: any;
  fileName: string;
  type: string;
  yangText: string;
  currentYangName: string;

  constructor(private logService: LogService) {
  }

  getLogs(): void {
    this.logService.getLogs()
      .subscribe(logs => {
        this.logs = logs;
        this.logs.sort((a, b) => {
          return a.updatedAt < b.updatedAt ? 1 : -1
        })
      });
  }

  onChange(e): void {
    const file = e.target.files[0];
    this.fileName = file.name;
    const formData = new FormData();
    formData.append('file', file);
    this.fileData = formData;
  }

  uploadFile(): void {
    this.logService.uploadModel(this.fileData, this.type)
      .subscribe(() => {
        this.modalOpened = false;
        this.getLogs();
      });
    this.fileData = null;
    this.type = '';
    this.fileName = '';
  }

  getLogUrl(id: number) {
    return this.logUrl.replace(':id', `${id}`);
  }

  showYangView(id: number,
               name: string): void {
    this.logService.getYangById(id)
      .subscribe(yangText => this.yangText = yangText);
    this.currentYangName = name;
    this.yangOpened = true;
  }

  ngOnInit() {
    this.getLogs();
  }
  closeUpdateFile() {
    this.fileData=null;
    this.modalOpened=false;
    this.type = '';
    this.fileName = '';
  }

  public downloadYang(log: Log): void {
    this.logService.getYangById(log.id)
      .subscribe(yangText => {
        const yangBlob = new Blob([yangText], {type: 'text/plain'});
        const data = window.URL.createObjectURL(yangBlob);
        const link = document.createElement('a');
        link.href = data;
        link.download = log.fileName;
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
      });
  }
}
