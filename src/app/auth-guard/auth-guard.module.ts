import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {AuthenticationService} from '../_service/authentication.service';
import {map} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class AuthGuardModule implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentAccessToken = this.authenticationService.currentAccessTokenValue;
    if (currentAccessToken) {
      return this.authenticationService.validateToken(currentAccessToken)
        .pipe(
          map(data => {
          if (data) {
            return true;
          } else {
            this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}}).then(() => false);
            return false;
          }
        }));
    }
  }
}
