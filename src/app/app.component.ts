import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from './_service/authentication.service';
import {UserService} from './_service/user.service';
import {User} from './_helpers/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app-network';
  public user: User;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService
  ) {
  }

  ngOnInit() {
    this.getUser();
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
    this.user = null;
  }

  getUser(): void {
    this.userService.getUser()
      .subscribe(user => this.user = user);
  }
}
