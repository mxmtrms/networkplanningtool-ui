import {Component, OnInit} from '@angular/core';
import {Project} from '../_helpers/project';
import {Router} from '@angular/router';
import {ProjectService} from '../_service/project.service';
import {UserService} from '../_service/user.service';
import {User} from '../_helpers/user';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  public projects: Project[];
  newProject: Project;
  modalOpened = false;
  public filteredProjects = [];
  private _searchValue: string;
  private user: User;

  set searchValue(value: string) {
    this._searchValue = value;
    this.filteredProjects = this.projects.filter(project => project.name.indexOf(this._searchValue) !== -1);
  }

  get searchValue() {
    return this._searchValue;
  }

  constructor(
    private projectService: ProjectService,
    private router: Router,
    private userService: UserService
  ) {
    this.newProject = {id: null, name: '', description: '', type: '', status: null, createdAt: null, updatedAt: null, owner: null, sheets: [], members: []};
  }

  ngOnInit() {
    this.userService.getUser().subscribe( user => {
        this.user = user;
      });
    this.getProjects();
  }

  getProjects(): void {
    this.projectService.getProjects()
      .subscribe(projects => {
        this.projects = projects;
        this.filteredProjects = projects;
      });
  }

  addProject(name: string, description: string, type: string): void {
    if (!name) {
      return;
    }
    this.projectService.addProject({name, description, type} as Project, this.user)
      .subscribe(() => {
        this.getProjects();
      });
  }

  deleteProject(project: Project): void {
    const projectId = project ? project.id : null;
    this.projectService.deleteProject(projectId).subscribe(() => {
      this.getProjects();
    });
  }

  canDelete(ownerId: string) {
    return this.user.id.toString() == ownerId;
  }

  createProject(): void {
    this.addProject(this.newProject.name,
      this.newProject.description,
      this.newProject.type);
    this.newProject.name = '';
    this.newProject.description = '';
    this.newProject.type = '';
    this.newProject.members = [];
  }

  getTypeColor(project) {
    if (project.type === 'public') {
      return 'green'
    }
    else if (project.type === 'private') {
      return 'red'
    }
  }

  getStatusColor(project) {
    if (project.status === 'archived') {
      return 'red'
    }
    else if (project.status === 'draft') {
      return 'blue'
    }
    else if (project.status === 'approved') {
      return 'green'
    }
  }
}
