import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {AuthenticationService} from '../_service/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  returnUrl: string;
  loginError: boolean = false;
  errorMsg: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    if (this.authenticationService.currentAccessTokenValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      usernameOrEmail: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.pattern(/^[a-zA-Z0-9!@#$%^&*()_]+$/)]
      ],
      password: ['', [
        Validators.required,
        Validators.minLength(8),
        Validators.pattern(/^[a-zA-Z0-9!@#$%^&*()_]+$/)]
      ]
    });

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get controls() {
    return this.loginForm.controls;
  }

  showError(msg: string) {
    this.errorMsg = msg;
    this.loginError = true;
    setTimeout(() => {
      this.loginError = false
    }, 5000)
  }

  onSubmit() {
    if (this.loginForm.valid) {
      const {usernameOrEmail, password} = this.controls;
      this.authenticationService.login(usernameOrEmail.value, password.value)
        .pipe(first())
        .subscribe(
          () => {
            this.router.navigate(['/']);
          }, error => {
            this.showError('Invalid Username/Email or Password!');
          });
    } else {
      if (this.loginForm.controls['usernameOrEmail'].invalid && (this.loginForm.controls['usernameOrEmail'] || this.loginForm.controls['usernameOrEmail'])) {
        if (this.loginForm.controls['usernameOrEmail'].errors.required) this.showError('Username/Email is required!')
        if (this.loginForm.controls['usernameOrEmail'].errors.minlength) this.showError('Username must be at least 3 characters long.')
        if (this.loginForm.controls['usernameOrEmail'].errors.pattern) this.showError('Username does not match template!')
      }
      if (this.loginForm.controls['password'].invalid && (this.loginForm.controls['password'] || this.loginForm.controls['password'])) {
        if (this.loginForm.controls['password'].errors.required) this.showError('Password is required!')
        if (this.loginForm.controls['password'].errors.required && this.loginForm.controls['usernameOrEmail'].errors.required) this.showError('Username/Email and Password is required!')
        if (this.loginForm.controls['password'].errors.minlength) this.showError('Password must be at least 8 characters long.')
        if (this.loginForm.controls['password'].errors.pattern) this.showError('Password does not match template!')
      }
    }
  }
}
