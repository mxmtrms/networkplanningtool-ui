import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {User} from '../_helpers/user';
import {HttpClient} from '@angular/common/http';
import {AuthenticationService} from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private authUrl = `${environment.apiUrl}/auth`;
  private projectsUrl = `${environment.apiUrl}/projects`;
  private usersUrl = `${environment.apiUrl}/users`;

  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService,
  ) {
  }

  getUser(): Observable<User> {
    const url = `${this.authUrl}/current/` + this.authenticationService.currentAccessTokenValue.accessToken;
    return this.http.get<User>(url);
  }

  getProjectMembers(projectId: string): Observable<User[]> {
    const url  = `${this.projectsUrl}/`+ projectId +`/members`;
    return this.http.get<User[]>(url);
  }

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl);
  }

  addMember(userId: number, projectId: string): Observable<User> {
    const url  = `${this.projectsUrl}/`+ projectId +`/members?userId=` + userId;
    return this.http.post<User>(url, userId);
  }
}
