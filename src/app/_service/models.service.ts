import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ModelsService {
  private projectsUrl = `${environment.apiUrl}/models`;

  constructor(
    private http: HttpClient
  ) {}

  getModel(type: string): Observable<any> {
    const url = `${this.projectsUrl}/${type}`;
    return this.http.get<any>(url);
  }
}
