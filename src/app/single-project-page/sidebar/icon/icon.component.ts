import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.css']
})
export class IconComponent implements OnInit {

  @Input() icon: Object;

  @Output() onClickIcon = new EventEmitter<Object>();    

  constructor() { }

  ngOnInit() {
  }

  clickIcon() {  
    this.onClickIcon.emit(this.icon);
  }

}
