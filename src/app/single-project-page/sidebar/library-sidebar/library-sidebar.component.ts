import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Project} from '../../../_helpers/project';

@Component({
  selector: 'app-library-sidebar',
  templateUrl: './library-sidebar.component.html',
  styleUrls: ['./library-sidebar.component.css']
})
export class LibrarySidebarComponent implements OnInit {

  icons=[
    {name: 'Location', image: 'location.svg', type: 'location'},
    {name: 'Cloud', image: 'cloud.svg', type: 'cloud'},
    {name: 'Link', image: 'link.svg', type: 'link'},
    {name: 'Switch', image: 'switch.svg', type: 'switch'},
    {name: 'PC', image: 'pc.svg', type: 'pc'},
    {name: 'Server', image: 'server.svg', type: 'server'},
    {name: 'Hub', image: 'hub.svg', type: 'hub'},
  ];

  @Input() project: Project;
  @Input() showError = false;
  @Input() errorMsg = '';
  @Output() onClickSidebar = new EventEmitter<Object>();

  constructor() { }

  ngOnInit() {
  }

  onClickIcons(icon: Object){
    this.onClickSidebar.emit(icon);
  }
}
