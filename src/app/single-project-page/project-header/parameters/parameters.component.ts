import { Component, OnInit, Input } from '@angular/core';
import { Project } from 'src/app/_helpers/project';

@Component({
  selector: 'app-parameters',
  templateUrl: './parameters.component.html',
  styleUrls: ['./parameters.component.css']
})
export class ParametersComponent implements OnInit {

  @Input() project: Project;
  

  constructor() { }

  ngOnInit() {
  }

  getColor(value) {
    if (value === 'public') {
      return 'green'
    }
    else if (value === 'private') {
      return 'red'
    }
    else if (value === 'archived') {
        return 'red'
    }
    else if (value === 'draft') {
      return 'blue'
    }
    else if (value === 'approved') {
      return 'green'
    }
  }

}
