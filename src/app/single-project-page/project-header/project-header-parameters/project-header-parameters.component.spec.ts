import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectHeaderParametersComponent } from './project-header-parameters.component';

describe('ProjectHeaderParametersComponent', () => {
  let component: ProjectHeaderParametersComponent;
  let fixture: ComponentFixture<ProjectHeaderParametersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectHeaderParametersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectHeaderParametersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
