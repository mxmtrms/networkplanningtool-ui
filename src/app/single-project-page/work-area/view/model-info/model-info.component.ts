import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-model-info',
  templateUrl: './model-info.component.html',
  styleUrls: ['./model-info.component.css']
})
export class ModelInfoComponent implements OnInit {
  @Input() selectedItem: any;
  @Output() deleteItem = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  deleteElement() {
    this.deleteItem.emit()
  }
}
